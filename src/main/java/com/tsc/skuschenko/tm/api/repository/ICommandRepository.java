package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}