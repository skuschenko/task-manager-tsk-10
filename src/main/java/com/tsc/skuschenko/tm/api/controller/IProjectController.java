package com.tsc.skuschenko.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
