package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

}
