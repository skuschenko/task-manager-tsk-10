package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project task);

    void remove(Project task);

    void clear();
}
