package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project Project);

    void remove(Project Project);

    void clear();

}
