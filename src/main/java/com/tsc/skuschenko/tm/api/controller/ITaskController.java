package com.tsc.skuschenko.tm.api.controller;

public interface ITaskController {

    void showList();

    void create();

    void clear();

}
