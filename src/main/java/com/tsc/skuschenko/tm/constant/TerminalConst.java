package com.tsc.skuschenko.tm.constant;

public interface TerminalConst {

    String TM_VERSION = "version";

    String TM_HELP = "help";

    String TM_ABOUT = "about";

    String TM_EXIT = "exit";

    String TM_INFO = "info";

    String TM_TASK_CREATE = "task-create";

    String TM_TASK_CLEAR = "task-clear";

    String TM_TASK_LIST = "task-list";

    String TM_PROJECT_CREATE = "project-create";

    String TM_PROJECT_CLEAR = "project-clear";

    String TM_PROJECT_LIST = "project-list";

    String TM_COMMANDS = "commands";

    String TM_ARGS = "arguments";

}
