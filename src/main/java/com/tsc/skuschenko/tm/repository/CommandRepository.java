package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.TM_ABOUT,
            ArgumentConst.ARG_ABOUT,
            "show about information"
    );

    private static final Command HELP = new Command(
            TerminalConst.TM_HELP,
            ArgumentConst.ARG_HELP,
            "show help information"
    );

    private static final Command VERSION = new Command(
            TerminalConst.TM_VERSION,
            ArgumentConst.ARG_VERSION,
            "show version application"
    );

    private static final Command EXIT = new Command(
            TerminalConst.TM_EXIT,
            null,
            "close program"
    );

    private static final Command INFO = new Command(
            TerminalConst.TM_INFO,
            ArgumentConst.ARG_INFO,
            "close program"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.TM_ARGS,
            null,
            "show arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.TM_COMMANDS,
            null,
            "show commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TM_TASK_CREATE,
            null,
            "create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TM_TASK_CLEAR,
            null,
            "clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TM_TASK_LIST,
            null,
            "show all tasks"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.TM_PROJECT_CREATE,
            null,
            "create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.TM_PROJECT_CLEAR,
            null,
            "clear all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.TM_PROJECT_LIST,
            null,
            "show all projects"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS, TASK_CREATE,
            TASK_CLEAR, TASK_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_LIST, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
