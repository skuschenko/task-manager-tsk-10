package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.IProjectController;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        final String showListInfo = "["
                + TerminalConst.TM_PROJECT_LIST.toUpperCase()
                + "]";
        System.out.println(showListInfo);
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        final String createInfo = "["
                + TerminalConst.TM_PROJECT_CREATE.toUpperCase()
                + "]";
        System.out.println(createInfo);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        final String clearInfo = "["
                + TerminalConst.TM_PROJECT_CLEAR.toUpperCase()
                + "]";
        System.out.println(clearInfo);
        projectService.clear();
        System.out.println("[OK]");
    }

}