package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.ITaskController;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        final String showListInfo = "["
                + TerminalConst.TM_TASK_LIST.toUpperCase()
                + "]";
        System.out.println(showListInfo);
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        final String createInfo = "["
                + TerminalConst.TM_TASK_CREATE.toUpperCase()
                + "]";
        System.out.println(createInfo);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        final String clearInfo = "["
                + TerminalConst.TM_TASK_CLEAR.toUpperCase()
                + "]";
        System.out.println(clearInfo);
        taskService.clear();
        System.out.println("[OK]");
    }

}
